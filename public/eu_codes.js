
const eu_codes_dict_old = [
    ["AT","AUT","Austria"],
    ["BE","BEL","Belgium"],
    ["BG", "BGR", "Bulgaria"],
    ["HR", "HRV", "Croatia"],
    ["CY","CYP","Cyprus"],
    ["CZ","CZE","Czech Republic"],
    ["DK", "DNK", "Denmark"],
    ["EE", "EST", "Estonia"],
    ["FI", "FIN", "Finland"],
    ["FR", "FRA", "France"],
    ["DE", "DEU", "Germany"],
    ["EL", "GRC", "Greece"],
    ["HU", "HUN", "Hungary"],
    ["IE", "IRL", "Ireland"],
    ["IT", "ITA", "Italy"],
    ["LV", "LVA", "Latvia"],
    ["LT", "LTU", "Lithuania"],
    ["LU", "LUX", "Luxembourg"],
    ["MT", "MLT", "Malta"],
    ["NL", "NLD", "Netherlands"],
    ["PL", "POL", "Poland"],
    ["PT", "PRT", "Portugal"],
    ["RO", "ROU", "Romania"],
    ["SK", "SVK", "Slovakia"],
    ["SI", "SVN", "Slovenia"],
    ["ES", "ESP", "Spain"],
    ["SE", "SWE", "Sweden"],
    ["UK", "GBR", "United Kingdom"],
    ["CH", "CHE", "Switzerland"],
    ["NO", "NOR", "Norway"]
]

const eu_codes_dict = [

    ["BE","BEL","Belgium"],
    ["BG", "BGR", "Bulgaria"],
    ["CZ","CZE","Czech Republic"],
    ["DK", "DNK", "Denmark"],
    ["DE", "DEU", "Germany"],
    ["EE", "EST", "Estonia"],
    ["IE", "IRL", "Ireland"],
    ["EL", "GRC", "Greece"],
    ["ES", "ESP", "Spain"],
    ["FR", "FRA", "France"],
    ["HR", "HRV", "Croatia"],
    ["IT", "ITA", "Italy"],
    ["CY","CYP","Cyprus"],
    ["LV", "LVA", "Latvia"],
    ["LT", "LTU", "Lithuania"],
    ["LU", "LUX", "Luxembourg"],
    ["HU", "HUN", "Hungary"],
    ["MT", "MLT", "Malta"],
    ["NL", "NLD", "Netherlands"],
    ["AT","AUT","Austria"],
    ["PL", "POL", "Poland"],
    ["PT", "PRT", "Portugal"],
    ["RO", "ROU", "Romania"],
    ["SI", "SVN", "Slovenia"],
    ["SK", "SVK", "Slovakia"],
    ["FI", "FIN", "Finland"],
    ["SE", "SWE", "Sweden"],
    ["UK", "GBR", "United Kingdom"],
    ["NO", "NOR", "Norway"],
    ["CH", "CHE", "Switzerland"]

]




class euCodes {

    constructor() {
        this.two = this.generate_array(0)
        this.three = this.generate_array(1)
        this.name = this.generate_array(2)
        this.labels = this.make_labels(this.generate_array(0))
    }

    make_labels(array) {
        array.unshift("Index")
        return array
    }

    generate_array(type) {
        const export_array = []
        for (var i=0;i<eu_codes_dict.length;i++) {
            export_array.push(eu_codes_dict[i][type])
        }
        return export_array
    }

    convert(code,type_0, type_1) {
        var index = null
        var match = {
            "two": this.two,
            "three": this.three,
            "name": this.name
        }
        type_0 = match[type_0]
        type_1 = match[type_1]

        for (var i=0;i<type_0.length;i++) {
            if (type_0[i] == code) {
                index = i
            }
        }
        return type_1[index]
        // return to this if you need it
    }

}

export default euCodes;