import 'ol/ol.css';

import Map from 'ol/Map';
import View from 'ol/View';
import {OSM, Vector as VectorSource} from 'ol/source';
import VectorLayer from 'ol/layer/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import {Fill, Stroke, Style, Text} from 'ol/style';
import {transform} from 'ol/proj';
import euCodes from '/eu_codes';
import State from '/State';
import generateForm from '/gen_form'



var CURRENT_DAY = 2;
var PAUSE = false;
var LOCAL_STATE = {}

const eu_codes = new euCodes()
const state = new State()

var display = document.getElementById("display");
var percentFade = 1

const play_button = document.getElementById("play")
play_button.onclick = function() {play(true)}
const reset_button = document.getElementById("reset")
reset_button.onclick = reset

const controls = document.getElementById("place_buttons")
var graph = null



init()

function init() {
  generateForm()
  display.innerHTML = getDate(CURRENT_DAY); 
  reset_button.style.pointerEvents = "none"
  reset_button.style.opacity = "50%"
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      const response = JSON.parse(this.responseText)
      console.log(response)
      state.fetchFrames(response)
      graph = new Dygraph(
        document.getElementById("graphs_wrap"),
        state.getGraph(CURRENT_DAY),
        {
          ylabel: "% Infected",
          xlabel: "Time",
          legend: "always",
          labelsDiv: document.getElementById("labels_wrap"),
          labels: eu_codes.labels
        }
      )
      vectorSource.changed()
    }
  };
  xmlhttp.open("POST", "/api", true);
  xmlhttp.setRequestHeader("Content-type", "application/json");
  xmlhttp.send(JSON.stringify(state.sendState(CURRENT_DAY)));

}




function play(initial) {
  if (initial) {

    PAUSE = false
    play_button.onclick = pause;
    play_button.innerHTML = "Loading..."
    controls.style.pointerEvents = "none"
    controls.style.opacity = "50%"
    reset_button.style.pointerEvents = "none"
    reset_button.style.opacity = "50%"

    const borders = document.getElementById("place_buttons")
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        play_button.innerHTML = "Pause"
        const response = JSON.parse(this.responseText)
        state.fetchFrames(response)

        return play(false)
      }
    };
    xmlhttp.open("POST", "/api", true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.send(JSON.stringify(state.sendState(CURRENT_DAY)));
  }
  else {
    setTimeout( function() {
      
      if (CURRENT_DAY < 730 && PAUSE === false) {
          display.innerHTML = getDate(CURRENT_DAY)
          CURRENT_DAY += 1
          vectorSource.changed()
          graph.updateOptions(
            {
              file: state.getGraph(CURRENT_DAY),
              labels: eu_codes.labels
            }
          )
          return play(false)
      }
    ;}, 600);
  }

}

function getDate(index) {
  const d = getDateObj(index)
  const string = d.toDateString()
  const day = d.getDate()
  const month = d.getMonth()+1
  const year = d.getFullYear()
  return day+"/"+month+"/"+year
}

function getDateObj(index) {
  const start = 1583020800
  const one_day = 86400
  const d = new Date(1000*(start + (index*one_day)))
  return d
}


function pause() {
  controls.style.pointerEvents = "auto"
  controls.style.opacity = "100%"
  reset_button.style.pointerEvents = "auto"
  reset_button.style.opacity = "100%"
  PAUSE = true;
  play_button.onclick = function() {play(true)}
  play_button.innerHTML = "Resume"
}

function reset() {
  CURRENT_DAY = 2
  state.clear()
  vectorSource.changed()
  generateForm()
  graph.updateOptions(
    {
      file: state.getGraph(CURRENT_DAY),
      labels: eu_codes.labels
    }
  )
  display.innerHTML = getDate(CURRENT_DAY)
  play_button.innerHTML = "Start"
}



function gradient(percentFade) {

  const startColor = {
    red: 199,
    green: 255,
    blue: 199
  }
  const endColor = {
    red: 255,
    green: 0,
    blue: 0
  }


  var diffRed = endColor.red - startColor.red;
  var diffGreen = endColor.green - startColor.green;
  var diffBlue = endColor.blue - startColor.blue;

  diffRed = (diffRed * percentFade) + startColor.red;
  diffGreen = (diffGreen * percentFade) + startColor.green;
  diffBlue = (diffBlue * percentFade) + startColor.blue;

  return "rgb("+diffRed+","+diffGreen+","+diffBlue+")"

}


var mr_worldwide = new Style({
  fill: new Fill({
    color: "rgba(255,255,255,0.2)"
  }),
  text: new Text()
});

function gen_style(code,selected) {
  return new Style({
    fill: new Fill({
      color: gradient(state.getFrame(CURRENT_DAY,code))
    }),
    stroke: new Stroke({
      color: "rgba(49, 159, 211,0.6)",
      width: 1
    }),
    text: new Text({
      text: state.getText(CURRENT_DAY,code, true),
      overflow: true,
      scale: 1
    })
  })
} 

const border_style = new Style({
  stroke: new Stroke({
    color: "rgba(0, 0, 0, 1)",
    width: 2
  }),
});


var vectorSource = new VectorSource({
    url: 'countries.json',
    format: new GeoJSON()
})

var vector_layer = new VectorLayer({
    source: vectorSource,
    style: function(feature) {
        var id = feature.getId()
        if (eu_codes.three.includes(id)) {
            var style = gen_style(eu_codes.convert(id,"three","two"),false)
            return style;
        }
        return mr_worldwide;
    }
});

var border_layer = new VectorLayer({
  source: vectorSource,
  style: function(feature) {
      var id = feature.getId()
      if (state.borders.includes(eu_codes.convert(id,"three","two"))) {
          var style = border_style
          return style;
      }
  }
});

const center = transform([10, 50], 'EPSG:4326', 'EPSG:3857');

var view = new View({
  center: center,
  zoom: 3.5
})

var map = new Map({
  view: view,
  layers: [
    vector_layer,
    border_layer
  ],
  target: 'map'
});


