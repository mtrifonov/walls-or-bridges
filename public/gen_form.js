import euCodes from '/eu_codes';
const eu_codes = new euCodes()

function generateToggles() {
    const tr = document.createElement("tr")
    tr.style.borderBottom = "2px solid #cccccc"
    tr.style.borderTop = "2px solid #cccccc"
    const borders = document.createElement("td")
    const labels = document.createElement("td")
    const social = document.createElement("td")
    
    labels.style.paddingTop = "25px"
    labels.style.paddingBottom = "25px"
    labels.style.verticalAlign = "middle"
    social.style.verticalAlign = "middle"
    borders.style.verticalAlign = "middle"


    const slider = document.createElement("input")
    slider.type = "range"
    slider.min = 0
    slider.max = 90
    slider.value = 0
    slider.oninput = function() {
      document.getElementById("slider_all").innerHTML = this.value+"%"
      const regulars = document.getElementsByClassName("reg_slider")
      const displays = document.getElementsByClassName("reg_slider_display")
      for (var i = 0; i< regulars.length; i++) {
          regulars[i].value = this.value
          displays[i].innerHTML = this.value+"%"
      }
    }

    const slider_display = document.createElement("div")
    slider_display.id = "slider_all"
    slider_display.innerHTML = "0%"

    slider_display.style.width = "50px"
    slider_display.style.marginLeft = "5px"
    slider_display.style.float = "right"

    const box = document.createElement("label")
    const d_c = document.createElement("input")
    const checkmark = document.createElement("span")
    d_c.type = "checkbox"
    d_c.id = "toggle_all_borders"
    d_c.onclick = function() {

        const value = this.checked
        const names = eu_codes.name
        const code = eu_codes.two
        console.log(value)
        for (var i = 0; i<names.length;i++) {
            const el = document.getElementById(names[i]+"_borders")
            const checked = el.checked
            console.log(checked)
            if (checked != value) {       
                el.click()
            }
        }

    }

    box.className = "container"
    checkmark.className = "checkmark"
    box.appendChild(d_c)
    box.appendChild(checkmark)
    

    const label = document.createElement("div")
    label.for = box.id
    label.innerHTML = "<b>All</b>"

    const social_box = document.createElement("div")
    social_box.style.display = "flex"
    social_box.style.alignItems = "center"
    social_box.appendChild(slider)
    social_box.appendChild(slider_display)
    social.appendChild(social_box)
    borders.appendChild(box)
    labels.appendChild(label)

    tr.appendChild(labels)
    tr.appendChild(social)
    tr.appendChild(borders)

    return tr
}

function generateHeaders() {
    var tr = document.createElement("tr")
    var borders = document.createElement("td")
    var labels = document.createElement("td")
    var social = document.createElement("td")
  
    borders.innerHTML = "Borders"
    borders.style.height = "30px"
    labels.innerHTML = "Country"
    social.innerHTML = "Social Distancing"
    tr.appendChild(labels)
    tr.appendChild(social)
    tr.appendChild(borders)
    return tr
}

function generateForm() {
    const countries = eu_codes.name
    const codes = eu_codes.two
    const form = document.createElement("form")
    const wrap = document.getElementById("place_buttons")
  
    wrap.innerHTML = ""

    wrap.appendChild(generateHeaders())
    wrap.appendChild(generateToggles())
  
    for (var i=0;i<countries.length;i++) {
  
      const tr = document.createElement("tr")
      const borders = document.createElement("td")

      const labels = document.createElement("td")
      const social = document.createElement("td")
  
      if (i==0) {
          labels.style.paddingTop = "15px"
          borders.style.paddingTop = "15px"
          social.style.paddingTop = "15px"
        }

      const slider = document.createElement("input")
      slider.id = countries[i]+"_social"
      slider.type = "range"
      slider.min = 0
      slider.max = 90
      slider.value = 0
      slider.className = "reg_slider"
      slider.oninput = function() {
        document.getElementById(this.id+"_display").innerHTML = this.value+"%"
      }
  
      const slider_display = document.createElement("div")
      slider_display.id = countries[i]+"_social_display"
      slider_display.innerHTML = "0%"
      slider_display.className = "reg_slider_display"
      slider_display.style.width = "50px"
      slider_display.style.marginLeft = "5px"
      slider_display.style.float = "right"
  
      const box = document.createElement("label")
      const d_c = document.createElement("input")
      const checkmark = document.createElement("span")
      d_c.type = "checkbox"
      d_c.id = countries[i]+"_borders"
      d_c.value = codes[i]
      box.className = "container"
      checkmark.className = "checkmark"
      box.appendChild(d_c)
      box.appendChild(checkmark)
      
  
      const label = document.createElement("div")
      label.for = box.id
      label.innerHTML = countries[i]

      const social_box = document.createElement("div")
      social_box.style.display = "flex"
      social_box.style.alignItems = "center"
      social_box.appendChild(slider)
      social_box.appendChild(slider_display)
      social.appendChild(social_box)

      borders.appendChild(box)
      labels.appendChild(label)
  
      tr.appendChild(labels)
      tr.appendChild(social)
      tr.appendChild(borders)
  
      wrap.appendChild(tr)
  
    }
  
  }


  export default generateForm;