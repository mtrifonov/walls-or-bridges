import euCodes from '/eu_codes';

const eu_codes = new euCodes()

class State {

    constructor() {
        this.borders = []
        this.R = {}
        this.current_state = {
        }

    };

    readState() {
        const names = eu_codes.name
        const code = eu_codes.two
        const borders = []
        const R = {}

        for (var i = 0; i<names.length;i++) {
            const checked = document.getElementById(names[i]+"_borders").checked
            const social = 100-document.getElementById(names[i]+"_social").value
            R[code[i]] = social
            if (checked) {
                borders.push(code[i])
            }

        }
        return [borders, R]
    }

    clear() {
        this.borders = []
        this.R = {}
        this.current_state = {
        }
    }


    sendState(CURRENT_DAY) {

        const state = this.readState()
        this.borders = state[0]
        this.R = state[1]

        this.current_state[CURRENT_DAY] = {
            "closed_borders": this.borders,
            "R": this.R
        }
        console.log(JSON.stringify(this.current_state))

        return this.current_state
    }

    fetchFrames(response) {
        this.frames = response["map"]

        const plot = response["plot"]
        for (var i = 0; i<plot.length;i++) {
            plot[i].unshift(i)
        }
        this.plot = plot
    }

  
    getGraph(CURRENT_DAY) {
        const sliced = this.plot.slice(0,CURRENT_DAY)
        console.log(sliced)
        return sliced
    }


    getFrame(CURRENT_DAY,code) {

        if (this.frames == null) {
            return 0
        }
        return this.frames[CURRENT_DAY][code]["infected_percentage"]/30

    }

    getText(CURRENT_DAY,code,selected) {
        if (this.frames == null  || selected == false) {
            return code
        }

        const infected = this.frames[CURRENT_DAY][code]["infected_total"]
        const magnitude = (number) => {
            if (number > 1000000) {
                return Math.round(number/1000000)+"m"
            }
            if (number > 1000) {
                return Math.round(number/1000)+"k"
            }
            return number

        }

        const string = `${code} \n ${magnitude(infected)}`
        return string
    }

}



export default State;